package com.ismail.usermanagement.request;

import com.ismail.usermanagement.enumeration.ERole;
import lombok.Data;

@Data
public class UserRequest {


    private String email;

    private String password;

    private ERole role;


}

