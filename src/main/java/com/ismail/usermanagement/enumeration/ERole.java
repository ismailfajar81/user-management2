package com.ismail.usermanagement.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ERole {
    MAKER,
    CHECKER,
    APPROVER;

    @JsonCreator
    public static ERole create(String value) {
        return ERole.valueOf(value.toUpperCase());
    }
}
