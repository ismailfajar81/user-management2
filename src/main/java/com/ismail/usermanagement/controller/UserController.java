package com.ismail.usermanagement.controller;

import com.ismail.usermanagement.entity.User;
import com.ismail.usermanagement.request.UserRequest;
import com.ismail.usermanagement.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/")
    public ResponseEntity<List<User>> getAllUser() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @PostMapping("/user")
    public ResponseEntity<User> createAuthor(@RequestBody UserRequest request) {
        return ResponseEntity.ok(userService.create(request));
    }

    @PatchMapping("/user/{userId}")
    public ResponseEntity<User> updateUser(@PathVariable("userId") UUID userId, @RequestBody UserRequest request) {
        return ResponseEntity.ok(userService.update(userId, request));
    }

    @DeleteMapping("/user/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable UUID userId) {
        userService.delete(userId);
        return ResponseEntity.ok().build();
    }
}

