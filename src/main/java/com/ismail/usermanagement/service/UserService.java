package com.ismail.usermanagement.service;

import com.ismail.usermanagement.entity.User;
import com.ismail.usermanagement.repository.UserRepository;
import com.ismail.usermanagement.request.UserRequest;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RequiredArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User create(UserRequest request) {
        User user1 = new User();
        BeanUtils.copyProperties(request, user1);
        return userRepository.save(user1);
    }

    public User update(UUID id, UserRequest request) {
        Optional<User> optionalMember = userRepository.findById(id);
        if (!optionalMember.isPresent()) {
            throw new EntityNotFoundException("Member not present in the database");
        }
        User user = optionalMember.get();
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        user.setRole(request.getRole());

        return userRepository.save(user);
    }

    public void delete(UUID id) {
        userRepository.deleteById(id);
    }
}
